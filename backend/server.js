const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

const transaction = require('./routes/transaction');
const user = require('./routes/user');

const app = express();

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
};

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
});

app.use(cors(corsOptions));

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

mongoose.set('createIndexes', true);

// Use Routes
app.use('/api/transaction', transaction);
app.use('/api/user', user);

const port = require('./config/keys').serverPort;

app.listen(port, () => console.log(`Server running on port ${port}`));
