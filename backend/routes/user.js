const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.get('/', async (req, res) => {
  console.log('testing');
  User.find()
    .then(users => res.json(users))
    .catch(err => res.status(500).json({ err }));
});

router.get('/:id', async (req, res) => {
  User.findById(req.params.id)
    .then(user => res.json(user))
    .catch(err => res.status(500).json({ err }));
});

router.post('/', async (req, res, next) => {
  const newUser = new User({
    name: req.body.name,
    email: req.body.email
  });
  newUser
    .save()
    .then(user => res.json(user))
    .catch(err => res.status(500).json({ err }));
});

module.exports = router;
