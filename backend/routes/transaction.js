const express = require('express');
const moment = require('moment');
const router = express.Router();
const Transaction = require('../models/Transaction');

const removeEmpty = obj => {
  Object.keys(obj).forEach(
    k => !obj[k] && obj[k] !== undefined && delete obj[k]
  );
  return obj;
};

const getDates = date => {
  const startOfMonth = moment(date)
    .startOf('month')
    .toDate();

  const endOfMonth = moment(date)
    .endOf('month')
    .toDate();

  return [startOfMonth, endOfMonth];
};

router.get('/', async (req, res) => {
  Transaction.find()
    .then(transactions => res.json(transactions))
    .catch(err => res.status(500).json({ err }));
});

router.get('/:id', async (req, res) => {
  Transaction.findById(req.params.id)
    .then(transaction => res.json(transaction))
    .catch(err => res.status(500).json({ err }));
});

router.post('/', async (req, res) => {
  const data = req.body;
  const newTransaction = new Transaction({
    ...data
  });
  newTransaction
    .save()
    .then(post => res.json(post))
    .catch(err => res.status(500).json({ err }));
});

router.post('/query', async (req, res) => {
  const props = removeEmpty(req.body);
  const dates = getDates(props.date);
  const queryObj = {
    ...props,
    date: { $gt: dates[0], $lte: dates[1] }
  };

  Transaction.find(queryObj)
    .then(transactions => res.json(transactions))
    .catch(err => res.status(500).json({ err }));
});

router.post('/summary', async (req, res) => {
  const props = removeEmpty(req.body);
  const dates = getDates(props.date);

  const aggregate = Transaction.aggregate([
    { $match: { ...props, date: { $gt: dates[0], $lt: dates[1] } } },
    { $group: { _id: '1', total: { $sum: '$amount' } } }
  ]);

  aggregate.then(data => {
    res.json(data);
  });
});

router.delete('/:id', async (req, res) => {
  Transaction.findById(req.params.id)
    .then(transaction => {
      transaction.remove().then(() => res.json({ success: true }));
    })
    .catch(err => res.status(500).json({ err }));
});

module.exports = router;
