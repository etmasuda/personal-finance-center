const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const TransactionSchema = new Schema({
  amount: {
    type: Number,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  currency: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: false
  },
  date: {
    type: Date,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true
  }
});

module.exports = Transaction = mongoose.model('Transaction', TransactionSchema);
