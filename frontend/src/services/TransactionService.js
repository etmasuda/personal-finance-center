import axios from 'axios';
import config from '../custom/config';

const URI = `${config.BackendUrl}/transaction`;

const get = () => {
  try {
    return axios.get(URI);
  } catch (error) {
    console.error(error);
  }
};

const post = data => {
  try {
    return axios.post(URI, data);
  } catch (error) {
    console.error(error);
  }
};

const remove = id => {
  try {
    return axios.delete(`${URI}/${id}`);
  } catch (error) {
    console.error(error);
  }
};

export { get, post, remove };
