import React, { useState, useEffect, useCallback } from 'react';

import { makeStyles } from '@material-ui/styles';

import { get, remove } from '../../services/TransactionService';

import TransactionToolbar from '../components/TransactionToolbar';

import TransactionTable from '../components/TransactionTable';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const TransactionPage = () => {
  const classes = useStyles();
  const [transactions, setTransactions] = useState([]);
  const [selectedTransactions, setSelectedTransactions] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await get();
      setTransactions(result.data);
    };

    fetchData();
  }, []);

  const deleteTransaction = useCallback(() => {
    selectedTransactions.forEach(transaction => remove(transaction));
  }, [selectedTransactions]);

  return (
    <div className={classes.root}>
      <TransactionToolbar deleteTransaction={deleteTransaction} />
      <div className={classes.content}>
        <TransactionTable
          transactions={transactions}
          selectedTransactions={selectedTransactions}
          setSelectedTransactions={setSelectedTransactions}
        />
      </div>
    </div>
  );
};

export default TransactionPage;
