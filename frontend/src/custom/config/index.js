import configDev from './config_dev';
import configProd from './config_prod';

export default process.env.NODE_ENV === 'production' ? configProd : configDev;
