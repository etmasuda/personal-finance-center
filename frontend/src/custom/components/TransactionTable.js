import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination
} from '@material-ui/core';
import { convertCompilerOptionsFromJson } from 'typescript';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));

const TransactionTable = props => {
  const {
    className,
    transactions,
    selectedTransactions,
    setSelectedTransactions,
    ...rest
  } = props;

  const classes = useStyles();

  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = event => {
    const { transactions } = props;

    let selectedTransactions;

    if (event.target.checked) {
      selectedTransactions = transactions.map(transaction => transaction._id);
    } else {
      selectedTransactions = [];
    }

    setSelectedTransactions(selectedTransactions);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedTransactions.indexOf(id);
    let newSelectedTransactions = [];

    if (selectedIndex === -1) {
      newSelectedTransactions = newSelectedTransactions.concat(
        selectedTransactions,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedTransactions = newSelectedTransactions.concat(
        selectedTransactions.slice(1)
      );
    } else if (selectedIndex === selectedTransactions.length - 1) {
      newSelectedTransactions = newSelectedTransactions.concat(
        selectedTransactions.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedTransactions = newSelectedTransactions.concat(
        selectedTransactions.slice(0, selectedIndex),
        selectedTransactions.slice(selectedIndex + 1)
      );
    }

    setSelectedTransactions(newSelectedTransactions);
  };

  const handlePageChange = (event, page) => {
    setPage(page);
  };

  const handleRowsPerPageChange = event => {
    setRowsPerPage(event.target.value);
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Checkbox
                      checked={
                        selectedTransactions.length === transactions.length
                      }
                      color="primary"
                      indeterminate={
                        selectedTransactions.length > 0 &&
                        selectedTransactions.length < transactions.length
                      }
                      onChange={handleSelectAll}
                    />
                  </TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Category</TableCell>
                  <TableCell>Amount</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {transactions
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(transaction => (
                    <TableRow
                      className={classes.tableRow}
                      hover
                      key={transaction._id}
                      selected={
                        selectedTransactions.indexOf(transaction._id) !== -1
                      }>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedTransactions.indexOf(transaction._id) !== -1
                          }
                          color="primary"
                          onChange={event =>
                            handleSelectOne(event, transaction._id)
                          }
                          value="true"
                        />
                      </TableCell>
                      <TableCell>
                        {moment(transaction.date).format('DD/MM/YYYY')}
                      </TableCell>
                      <TableCell>{transaction.category}</TableCell>
                      <TableCell>{transaction.amount}</TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
      <CardActions className={classes.actions}>
        <TablePagination
          component="div"
          count={transactions.length}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handleRowsPerPageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[10, 20, 30]}
        />
      </CardActions>
    </Card>
  );
};

TransactionTable.propTypes = {
  className: PropTypes.string,
  transactions: PropTypes.array.isRequired
};

export default TransactionTable;
