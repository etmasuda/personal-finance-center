import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  }
}));

const TransactionToolbar = props => {
  const { className, deleteTransaction, ...rest } = props;

  const classes = useStyles();

  const onDelete = () => {
    props.deleteTransaction();
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <div className={classes.row}>
        <span className={classes.spacer} />
        <Button color="primary" variant="contained" onClick={onDelete}>
          Delete
        </Button>
      </div>
    </div>
  );
};

TransactionToolbar.propTypes = {
  className: PropTypes.string
};

export default TransactionToolbar;
